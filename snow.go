/*
Package snow is the main service now library package
*/
package snow

import (
	"errors"
	"fmt"
	"os"
	"strings"

	servicenowsdkgo "github.com/michaeldcanady/servicenow-sdk-go"
	"github.com/michaeldcanady/servicenow-sdk-go/credentials"
	tableapi "github.com/michaeldcanady/servicenow-sdk-go/table-api"
)

// SNow is the main object to interact with service now
type SNow struct {
	instance string
	username string
	password string
	Client   *servicenowsdkgo.ServiceNowClient
}

// WithUsername sets the username
func WithUsername(u string) func(*SNow) {
	return func(s *SNow) {
		s.username = u
	}
}

// WithPassword sets the password
func WithPassword(p string) func(*SNow) {
	return func(s *SNow) {
		s.password = p
	}
}

// WithInstance sets the instance
func WithInstance(i string) func(*SNow) {
	return func(s *SNow) {
		s.instance = i
	}
}

func mustNew(opts ...func(*SNow)) *SNow {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

// New returns a new SNow object and optional error
func New(opts ...func(*SNow)) (*SNow, error) {
	s := &SNow{
		username: os.Getenv("SN_USER"),
		password: os.Getenv("SN_PASS"),
		instance: envOrDefault("SN_INSTANCE", "duke"),
	}
	for _, opt := range opts {
		opt(s)
	}
	if s.instance == "" {
		return nil, errors.New("must set instance")
	}
	if s.username == "" {
		return nil, errors.New("must set username")
	}
	if s.password == "" {
		return nil, errors.New("must set password")
	}
	s.Client = servicenowsdkgo.NewServiceNowClient(
		credentials.NewUsernamePasswordCredential(s.username, s.password),
		s.instance)
	return s, nil
}

// MustGetRecord panics if the record cannot be found
func (s *SNow) MustGetRecord(table string, params *tableapi.TableRequestBuilderGetQueryParameters) *tableapi.TableEntry {
	got, err := s.GetRecord(table, params)
	if err != nil {
		panic(err)
	}
	return got
}

// GetRecord returns a single record entry. If 0 or more than 1 entries are found, an error is also returned
func (s *SNow) GetRecord(table string, params *tableapi.TableRequestBuilderGetQueryParameters) (*tableapi.TableEntry, error) {
	got, err := s.Client.Now().Table(table).Get(params)
	if err != nil {
		return nil, err
	}
	if len(got.Result) != 1 {
		return nil, fmt.Errorf("error querying table %v, got %v results instead of 1", table, len(got.Result))
	}
	return got.Result[0], nil
}

// Get returns all entries matching a given params
func (s *SNow) Get(table string, params *tableapi.TableRequestBuilderGetQueryParameters) (
	*tableapi.TableCollectionResponse2[tableapi.TableEntry],
	error,
) {
	return s.Client.Now().Table(table).Get(params)
}

func buildSysparmQuery(s []string) string {
	return strings.Join(s, "^")
}

func envOrDefault(e, d string) string {
	got := os.Getenv(e)
	if got == "" {
		return d
	}
	return got
}
