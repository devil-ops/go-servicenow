/*
Package cmd is the cli package for interactions
*/
package cmd

import (
	"io"
	"log/slog"
	"os"
	"time"

	"github.com/charmbracelet/log"
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	snow "gitlab.oit.duke.edu/devil-ops/go-servicenow"
)

var (
	sn      *snow.SNow
	logger  *slog.Logger
	verbose bool
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:           "snow",
	Short:         "Interact with Service Now API",
	SilenceUsage:  true,
	SilenceErrors: true,
	PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
		var err error
		sn, err = snow.New()
		if err != nil {
			return err
		}
		return gout.ApplyCobra(gout.GetGout(), cmd, nil)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logger.Error("fatal error occurred", "error", err)
		os.Exit(1)
	}
}

func init() {
	panicIfErr(gout.BindCobra(rootCmd, nil))
	setupLogging(os.Stderr)
}

func setupLogging(w io.Writer) {
	if w == nil {
		panic("must set writer")
	}

	logger = slog.New(log.NewWithOptions(w, newLoggerOpts()))
	slog.SetDefault(logger)
}

func newLoggerOpts() log.Options {
	logOpts := log.Options{
		ReportTimestamp: true,
		TimeFormat:      time.Kitchen,
		Prefix:          "snow ❄️ ",
		Level:           log.InfoLevel,
	}
	if verbose {
		logOpts.Level = log.DebugLevel
	}

	return logOpts
}
