package cmd

import (
	"log/slog"
	"time"

	"github.com/charmbracelet/huh/spinner"
	"github.com/charmbracelet/lipgloss"
	"github.com/drewstinnett/gout/v2"
	tableapi "github.com/michaeldcanady/servicenow-sdk-go/table-api"
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use:   "get [table]",
	Short: "Get tables or records inside of tables. If no table name is given, dumps the sys_db_object table, which is a list of all available tables",
	Args:  cobra.MaximumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		start := time.Now()
		table := "sys_db_object"
		if len(args) > 0 {
			table = args[0]
		}
		// Set up the params:
		params := &tableapi.TableRequestBuilderGetQueryParameters{
			Limit: mustGetCmd[int](cmd, "limit"),
		}
		if mustGetCmd[bool](cmd, "display-value") {
			params.DisplayValue = tableapi.ALL
		}
		var got *tableapi.TableCollectionResponse2[tableapi.TableEntry]

		err := spinner.New().
			Title("Querying ServiceNow...").
			TitleStyle(lipgloss.NewStyle()).
			Action(func() {
				var err error
				got, err = sn.Client.Now().Table(table).Get(params)
				if err != nil {
					panic(err)
				}
			}).Run()
		if err != nil {
			return err
		}

		gout.MustPrint(got.Result)
		slog.Info("found results", "count", len(got.Result), "took", time.Since(start))
		return nil
	},
}

func init() {
	rootCmd.AddCommand(getCmd)
	getCmd.PersistentFlags().IntP("limit", "l", 0, "limit to N number results")
	getCmd.PersistentFlags().Bool("display-value", false, "include display values in results")
}
