/*
Package main is the main cli process
*/
package main

import "gitlab.oit.duke.edu/devil-ops/go-servicenow/cmd/snow/cmd"

func main() {
	cmd.Execute()
}
