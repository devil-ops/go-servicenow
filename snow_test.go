package snow

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewClient(t *testing.T) {
	os.Clearenv()
	_, err := New()
	require.EqualError(t, err, "must set username")
	_, err = New(WithInstance("foo"), WithUsername("some-user"))
	require.EqualError(t, err, "must set password")
	_, err = New(WithInstance("foo"), WithUsername("some-user"), WithPassword("some-password"))
	require.NoError(t, err)

	require.NotNil(t, mustNew(WithInstance("foo"), WithUsername("some-user"), WithPassword("some-password")))
}

func TestBuildSysParms(t *testing.T) {
	require.Equal(t, "foo^bar", buildSysparmQuery([]string{"foo", "bar"}))
}
